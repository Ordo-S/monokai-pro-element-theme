Element theme based of the [Monokai Pro](https://monokai.pro/) color scheme. 

![Screenshot of Theme](images/element-monokai-pro.png)
